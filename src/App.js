import React from 'react';
import styled from 'styled-components';

import { media } from 'config';

import CommonContainer from 'common/Container';

import Table from 'components/Table';
import Search from 'components/Search';

const Container = styled(CommonContainer)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  position: relative;
  padding: 0 20%;

  ${media.tablet`
    padding: 10px 40px;
  `}
`;

function App() {
  return (
    <Container>
      <Search />
      <Table />
    </Container>
  );
}

export default App;
