import React, { useState } from 'react';
import { connect } from 'react-redux';
import { func } from 'prop-types';

import { listenerActions, workerActions } from 'store/actions';

import {
  Search,
  Button,
  Container,
} from './styled';

const enhance = connect();

const Element = ({
  dispatch,
}) => {
  const [term, setTerm] = useState('');

  return (
    <Container>
      <Search
        placeholder="Start typing to searcht the table below..."
        value={term}
        onChange={(event) => {
          const value = event.target.value || '';
          setTerm(value);
          if (value === '') dispatch({ type: workerActions.CLEAR_SEARCH_TERM });
        }}
        onSearch={(value) => dispatch({
          type: listenerActions.SET_SEARCH_TERM_REQUEST,
          payload: { term: value },
        })}
        enterButton
      />
      <Button
        onClick={() => {
          setTerm('');
          dispatch({
            type: workerActions.CLEAR_SEARCH_TERM,
          });
        }}
      >
        Clear
      </Button>
    </Container>
  );
};

Element.propTypes = {
  dispatch: func.isRequired,
};

export default enhance(Element);
