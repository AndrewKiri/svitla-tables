import styled from 'styled-components';

import CommonContainer from 'common/Container';

import {
  Button as CommonButton,
  Input,
} from 'antd';

const { Search: CommonSearch } = Input;

export const Search = styled(CommonSearch)``;

export const Button = styled(CommonButton)`
  margin-left: 30px;
`;

export const Container = styled(CommonContainer)`
  margin-top: 32px;
`;
