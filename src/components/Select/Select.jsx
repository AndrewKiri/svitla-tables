import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import {
  arrayOf,
  shape,
  array,
  string,
  func,
} from 'prop-types';

import {
  statusesSelector,
  statusPairsSelector,
} from 'store/selectors';

import { listenerActions } from 'store/actions';

import {
  Select,
  Option,
} from './styled';

const enhance = connect(
  createStructuredSelector({
    statuses: statusesSelector,
    statusPairs: statusPairsSelector,
  }),
);

const Element = ({
  currentStatus,
  statusPairs,
  statuses,
  dispatch,
  id,
}) => (
  <Select
    style={{ width: 140 }}
    defaultValue={statusPairs.find((pair) => pair[1] === currentStatus)}
    onSelect={(status) => dispatch({
      type: listenerActions.SET_STATUS_REQUEST,
      payload: { status, id },
    })}
  >
    {Object.keys(statuses).map((key) => (
      <Option key={key} value={key}>
        {statuses[key]}
      </Option>
    ))}
  </Select>
);

Element.propTypes = {
  currentStatus: string.isRequired,
  statuses: shape({}).isRequired,
  statusPairs: arrayOf(array).isRequired,
  dispatch: func.isRequired,
  id: string.isRequired,
};

export default enhance(Element);
