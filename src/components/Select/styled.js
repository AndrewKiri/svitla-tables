import {
  Select as CommonSelect,
} from 'antd';

const { Option: CommonOption } = CommonSelect;

const Select = CommonSelect;
const Option = CommonOption;

export {
  Select,
  Option,
};
