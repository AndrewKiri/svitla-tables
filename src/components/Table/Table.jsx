import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { arrayOf, shape } from 'prop-types';

import {
  columnsSelector,
  dataSourceSelector,
} from 'store/selectors';

import {
  Table,
  Column,
} from './styled';

import Select from '../Select';

const enhance = connect(
  createStructuredSelector({
    columns: columnsSelector,
    dataSource: dataSourceSelector,
  }),
);

const Element = ({
  columns,
  dataSource,
}) => (
  <Table
    dataSource={dataSource}
    pagination={false}
    bordered
  >
    {columns.map((column) => {
      if (column.key === 'status') {
        return (
          <Column
            {...column}
            render={(currentStatus, record) => (
              <Select currentStatus={currentStatus} id={record.key} />
            )}
          />
        );
      }

      return (
        <Column {...column} />
      );
    })}
  </Table>
);

Element.propTypes = {
  columns: arrayOf(shape({})).isRequired,
  dataSource: arrayOf(shape({})).isRequired,
};

export default enhance(Element);
