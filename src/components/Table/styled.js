import styled from 'styled-components';

import { Table as CommonTable } from 'antd';

const { Column: CommonColumn } = CommonTable;

export const Table = styled(CommonTable)`
  margin-top: 24px;
`;

export const Column = CommonColumn;
