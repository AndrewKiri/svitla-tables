import { css } from 'styled-components';

const sizes = {
  desktop: 1200,
  tablet: 992,
  phone: 768,
};

const media = Object.keys(sizes).reduce((accumulator, label) => {
  accumulator[label] = (...args) => css`
    @media (max-width: ${sizes[label]}px) {
      ${css(...args)}
    }
  `;
  return accumulator;
}, {});

export {
  media,
};
