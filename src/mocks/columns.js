export default [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Role',
    dataIndex: 'role',
    key: 'role',
  },
  {
    title: 'Connected on',
    dataIndex: 'connected',
    key: 'connected',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
];
