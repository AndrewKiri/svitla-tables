import roles from 'mocks/roles';
import statuses from 'mocks/statuses';

export default [
  {
    key: '1',
    name: 'Andrew Kirichok',
    role: roles.ENGINEER,
    connected: '2010-08-08',
    status: statuses.HIRE,
  },
  {
    key: '2',
    name: 'Brad Pitt',
    role: roles.CUSTOMER_SUPPORT,
    connected: '2013-06-08',
    status: statuses.SCHEDULED,
  },
  {
    key: '3',
    name: 'Leonardo DiCaprio',
    role: roles.SALES,
    connected: '2005-04-03',
    status: statuses.SCREEN,
  },
  {
    key: '4',
    name: 'Johnny Depp',
    role: roles.MANAGER,
    connected: '2015-02-01',
    status: statuses.EXPLORED,
  },
  {
    key: '5',
    name: 'Robert De Niro',
    role: roles.ENGINEER,
    connected: '2010-09-15',
    status: statuses.HIRE,
  },
  {
    key: '6',
    name: 'Clint Eastwood',
    role: roles.CUSTOMER_SUPPORT,
    connected: '2012-12-12',
    status: statuses.EXPLORED,
  },
  {
    key: '7',
    name: 'Will Smith',
    role: roles.SALES,
    connected: '2014-04-18',
    status: statuses.SCHEDULED,
  },
  {
    key: '8',
    name: 'Tom Cruise',
    role: roles.ENGINEER,
    connected: '2000-10-01',
    status: statuses.HIRE,
  },
  {
    key: '9',
    name: 'Christian Bale',
    role: roles.MANAGER,
    connected: '2017-07-05',
    status: statuses.HIRE,
  },
  {
    key: '10',
    name: 'Matt Damon',
    role: roles.SALES,
    connected: '2010-08-16',
    status: statuses.SCREEN,
  },
];
