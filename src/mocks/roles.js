export default {
  ENGINEER: 'Engineer',
  SALES: 'Sales',
  CUSTOMER_SUPPORT: 'Customer Support',
  MANAGER: 'Manager',
};
