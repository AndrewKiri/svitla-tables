export default {
  SCREEN: 'Screen',
  SCHEDULED: 'Scheduled',
  EXPLORED: 'Explored',
  HIRE: 'Hire',
};
