const prefix = 'LISTENER';

export default {
  SET_STATUS_REQUEST: `${prefix}/SET_STATUS_REQUEST`,
  SET_SEARCH_TERM_REQUEST: `${prefix}/SET_SEARCH_TERM_REQUEST`,
};
