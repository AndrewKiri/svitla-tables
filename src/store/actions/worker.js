const prefix = 'WORKER';

export default {
  SET_STATUS: `${prefix}/SET_STATUS`,
  SET_SEARCH_TERM: `${prefix}/SET_SEARCH_TERM`,
  CLEAR_SEARCH_TERM: `${prefix}/CLEAR_SEARCH_TERM`,
};
