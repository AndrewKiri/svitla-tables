import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { logger } from 'redux-logger';
import reducers from './reducers';
import rootSaga from './sagas';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware(rootSaga);

const middleware = composeEnhancers(applyMiddleware(logger, sagaMiddleware));

const store = createStore(reducers, middleware);

sagaMiddleware.run(rootSaga);

export default store;
