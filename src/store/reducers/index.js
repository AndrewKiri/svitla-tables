import produce from 'immer';

import roles from 'mocks/roles';
import statuses from 'mocks/statuses';
import records from 'mocks/records';
import columns from 'mocks/columns';

import { workerActions } from '../actions';

const initialState = {
  statuses,
  roles,
  columns,
  records,
  search: {
    term: '',
    results: [],
  },
};

export default (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case workerActions.SET_STATUS: {
      const { status, id } = action.payload;
      const recordIndex = draft.records.findIndex((el) => el.key === id);

      draft.records[recordIndex].status = status;
      break;
    }

    case workerActions.SET_SEARCH_TERM: {
      const { term, results } = action.payload;

      draft.search.term = term;
      draft.search.results = results;

      break;
    }

    case workerActions.CLEAR_SEARCH_TERM: {
      draft.search.term = '';
      draft.search.results = [];

      break;
    }

    default:
      break;
  }
});
