import { takeEvery } from 'redux-saga/effects';
import { listenerActions } from '../actions';

import {
  setStatus,
  setSearchTerm,
} from './worker';

export default function* () {
  yield takeEvery(listenerActions.SET_STATUS_REQUEST, setStatus);
  yield takeEvery(listenerActions.SET_SEARCH_TERM_REQUEST, setSearchTerm);
}
