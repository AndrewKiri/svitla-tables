import { put } from 'redux-saga/effects';
import Fuse from 'fuse.js';

import records from 'mocks/records';
import { workerActions } from '../actions';

const options = {
  shouldSort: true,
  tokenize: false,
  matchAllTokens: true,
  findAllMatches: true,
  threshold: 0.4,
  location: 0,
  distance: 100,
  maxPatternLength: 64,
  minMatchCharLength: 1,
  keys: [
    'name',
    'role',
    'connected',
    'status',
  ],
};

const fuse = new Fuse(records, options);

function* setStatus(action) {
  const { status, id } = action.payload;
  const shouldPut = status && status.length > 0 && id;

  try {
    if (shouldPut) {
      yield put({
        type: workerActions.SET_STATUS,
        payload: { status, id },
      });
    }
  } catch (error) {
    yield put({
      type: 'ERROR',
      payload: { ...action, error },
    });
  }
}

function* setSearchTerm(action) {
  const { term } = action.payload;

  try {
    if (term && term.length > 0) {
      yield put({
        type: workerActions.SET_SEARCH_TERM,
        payload: { term, results: fuse.search(term) },
      });
    }
  } catch (error) {
    yield put({
      type: 'ERROR',
      payload: { ...action, error },
    });
  }
}

export {
  setStatus,
  setSearchTerm,
};
