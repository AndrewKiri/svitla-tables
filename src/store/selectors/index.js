import { createSelector } from 'reselect';
import toPairs from 'lodash/toPairs';

export const columnsSelector = (state) => state.columns;
export const recordsSelector = (state) => state.records;
export const rolesSelector = (state) => state.roles;
export const statusesSelector = (state) => state.statuses;
export const searchResultsSelector = (state) => state.search.results;
export const searchTermSelector = (state) => state.search.term;

export const statusPairsSelector = createSelector(
  statusesSelector,
  (statuses) => toPairs(statuses),
);

export const dataSourceSelector = createSelector(
  searchTermSelector,
  recordsSelector,
  searchResultsSelector,
  (term, records, results) => {
    const hasTerm = term.length > 0;

    if (!hasTerm) return records;
    return results;
  },
);
